package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const (
	flagCompile       = "compile"
	flagJavaPath      = "javaPath"
	flagMavenPath     = "mavenPath"
	flagMavenRepoPath = "mavenLocalRepository"

	pathBash     = "/bin/bash"
	pathJarsList = "/tmp/jars.list"
	pathOutput   = "/tmp/findSecurityBugs.xml"
	pathFsb      = "/findsecbugs-cli"
	pathPlugins  = pathFsb + "/lib/findsecbugs-plugin.jar"
	pathInclude  = pathFsb + "/include.xml"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolTFlag{
			Name:  flagCompile,
			Usage: "Compile Java code. It's not needed if the code is already compiled.",
		},
		cli.StringFlag{
			Name:   flagJavaPath,
			Usage:  "Define path to java executable.",
			Value:  "/usr/bin/java",
			EnvVar: "JAVA_PATH",
		},
		cli.StringFlag{
			Name:   flagMavenPath,
			Usage:  "Define path to mvn executable.",
			Value:  "/usr/share/maven/bin/mvn",
			EnvVar: "MAVEN_PATH",
		},
		cli.StringFlag{
			Name:   flagMavenRepoPath,
			Usage:  "Define path to maven local repository.",
			Value:  "/root/.m2",
			EnvVar: "MAVEN_REPO_PATH",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	// compile Java source code if needed
	if c.BoolT(flagCompile) {
		err := setupCmd(exec.Command(pathBash, c.String(flagMavenPath), "compile")).Run()
		if err != nil {
			return nil, err
		}

	}

	// Build list of JARs
	if err := buildJarsList(c.String(flagMavenRepoPath)); err != nil {
		return nil, err
	}

	// FSB CLI arguments
	// See https://github.com/find-sec-bugs/find-sec-bugs/wiki/CLI-Tutorial
	var args = []string{
		"-cp",
		pathFsb + "/lib/*",
		"edu.umd.cs.findbugs.LaunchAppropriateUI",
		"-pluginList", pathPlugins,
		"-include", pathInclude,
		"-xml:withMessages",
		"-auxclasspathFromFile", pathJarsList,
		"-output", pathOutput,
	}

	// Append target directories. They contain the generated .class files.
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() && info.Name() == "target" {
			args = append(args, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	// Run FSB CLI
	if err := setupCmd(exec.Command(c.String(flagJavaPath), args...)).Run(); err != nil {
		return nil, err
	}

	return os.Open(pathOutput)
}

func buildJarsList(mavenRepoPath string) error {
	f, err := os.OpenFile(pathJarsList, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	return filepath.Walk(mavenRepoPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filepath.Ext(info.Name()) == ".jar" {
			if _, err := fmt.Fprintln(f, path); err != nil {
				return err
			}
		}
		return nil
	})
}
